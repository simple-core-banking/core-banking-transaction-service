package com.b2camp.teamc.transactionservice.teamc.models;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import com.b2camp.teamc.transactionservice.teamc.models.references.MCif;
import com.b2camp.teamc.transactionservice.teamc.models.references.MSavingProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_saving_account")
@SuperBuilder
public class TSavingAccount extends BaseEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id",unique = true, nullable = false,length = 50)
        private Long id;

        @ManyToOne(targetEntity = MCif.class,cascade=CascadeType.ALL)
        @JoinColumn(name = "cif_id", referencedColumnName = "id")
        private MCif mcif;

        @ManyToOne(targetEntity = MSavingProduct.class,cascade=CascadeType.ALL)
        @JoinColumn(name = "m_saving_product_id", referencedColumnName = "id")
        private MSavingProduct mSavingProduct;

        @Column(name = "account_number", unique = true, nullable = false)
        private String accountNumber;

        @Column(name = "balance")
        private BigDecimal balance;
}
