package com.b2camp.teamc.transactionservice.teamc.dtos.requests;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MDepositBillyetRequest {
    private String bilYetNumber;
    private String tDepositAccount;
}
