package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TSavingAccountRepo extends JpaRepository<TSavingAccount, Long> {
    @Query(value="SELECT * FROM t_saving_account tsa "
            + "WHERE tsa.id = :id " + "AND tsa.is_deleted = false", nativeQuery= true)
    TSavingAccount findByIdTrue(@Param("id") Long id);


    Optional<List<TSavingAccount>> findByAccountNumberIn(List<String> accountNumber);
    Optional<TSavingAccount> findByAccountNumber(String accountNumber);

    @Query(value = " Select * from t_deposit_account tda "
            + "WHERE tda.t_saving_account_id = :tSavingAccountId ", nativeQuery = true)
    TSavingAccount findByTsaving(@Param("tSavingAccountId")Long tSavingAccount);
//    //Line 16 dan 17 untuk kebutuhan di transaction service
}
