package com.b2camp.teamc.transactionservice.teamc.services.impl;

import com.b2camp.teamc.transactionservice.teamc.constant.Constant;
import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TDepositAccountRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TDepositAccountResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import com.b2camp.teamc.transactionservice.teamc.repositories.*;
import com.b2camp.teamc.transactionservice.teamc.services.TDepositAccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
@AllArgsConstructor
public class TDepositAccountServiceImpl implements TDepositAccountService {


    private TDepositAccountRepo tDepositAccountRepo;

    private MCifRepo mCifRepo;

    private TSavingAccountRepo tSavingAccountRepo;

    private MDepositProductRepo depositProductRepo;

    private RStatusRepo rStatusRepo;

    private ModelMapper mapper;
    private ObjectMapper objectMapper;



    @Override
    @Transactional
    public TDepositAccountResponse add(TDepositAccountRequest tDepositAccountRequest) {
        TDepositAccount createDataTDeposit = TDepositAccount.builder()
                .mCif(mCifRepo.findById(tDepositAccountRequest.getMCif()).get())
                .startDate(new Date())
                .nominal(BigDecimal.valueOf(0))
                .timePeriod(tDepositAccountRequest.getTimePeriod())
                .rStatus(rStatusRepo.findById(tDepositAccountRequest.getRStatus()).get())
                .mDepositProduct(depositProductRepo.findById(tDepositAccountRequest.getMDepositProduct()).get())
                .deleted(Boolean.FALSE)
                .build();

        var savingCreateDataTDeposit = tDepositAccountRepo.save(createDataTDeposit);

        TDepositAccount processCreateAccountNumber = savingCreateDataTDeposit;
        processCreateAccountNumber.setAccountNumber("B2" + savingCreateDataTDeposit.getId());

        return TDepositAccountResponse.builder()
                .id(savingCreateDataTDeposit.getId())
                .mCif(savingCreateDataTDeposit.getMCif())
                .savingAccountId(savingCreateDataTDeposit.getTSavingAccount())
                .productId(savingCreateDataTDeposit.getMDepositProduct())
                .accountNumber(savingCreateDataTDeposit.getAccountNumber())
                .nominal(savingCreateDataTDeposit.getNominal())
                .startDate(savingCreateDataTDeposit.getStartDate())
                .timePeriod(savingCreateDataTDeposit.getTimePeriod())
                .rStatus(savingCreateDataTDeposit.getRStatus())
                .dueDate(savingCreateDataTDeposit.getDueDate())
                .build();
    }



    @Async
    @Scheduled(fixedRate = 60000)
    @Override
    @Transactional
    public void update() {
        Pageable paging = PageRequest.of(0, 1);
        List<TDepositAccount> findDepositAccount = tDepositAccountRepo.findDateIsNull(paging);
        if (!findDepositAccount.isEmpty()) {
            try {
                //TimeUnit.SECONDS.sleep(60000);
                Thread.sleep(60000);
                findDepositAccount.forEach(dataUpdate -> {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(dataUpdate.getStartDate());

                    cal.add(Calendar.YEAR, dataUpdate.getTimePeriod());
                    Date dueDate = cal.getTime();
                    log.info("Due Date : " + dueDate);
                    dataUpdate.setDueDate(dueDate);

                    TDepositAccount saveData = tDepositAccountRepo.save(dataUpdate);
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }




    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> softDelete = new HashMap<>();
        try {
            TDepositAccount dataDeposit = tDepositAccountRepo.findByIdTrue(id);
            dataDeposit.setDeleted(Boolean.TRUE);

            tDepositAccountRepo.save(dataDeposit);
            softDelete.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            softDelete.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            softDelete.put(Constant.DATA_STRING, "Deleted");
        } catch (Exception e) {
            softDelete.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            softDelete.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return softDelete;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TDepositAccount> getJoinInformation() {
        return (List<TDepositAccount>) tDepositAccountRepo.findAll();
    }

    @Override
    public TDepositAccountResponse getById(Long id) {
        var getDepositAccountData = tDepositAccountRepo.findById(id).get();
        return TDepositAccountResponse.builder()
                .id(getDepositAccountData.getId())
                .mCif(getDepositAccountData.getMCif())
                .savingAccountId(getDepositAccountData.getTSavingAccount())
                .accountNumber(getDepositAccountData.getAccountNumber())
                .timePeriod(getDepositAccountData.getTimePeriod())
                .startDate(getDepositAccountData.getStartDate())
                .dueDate(getDepositAccountData.getDueDate())
                .rStatus(getDepositAccountData.getRStatus())
                .productId(getDepositAccountData.getMDepositProduct())
                .nominal(getDepositAccountData.getNominal())
                .build();
    }
}
