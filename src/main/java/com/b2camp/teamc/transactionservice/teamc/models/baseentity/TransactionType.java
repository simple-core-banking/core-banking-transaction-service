package com.b2camp.teamc.transactionservice.teamc.models.baseentity;

public enum TransactionType {
    SETORAN, PENARIKAN, TRANSFER, PEMBAYARAN, PEMBELIAN
}
