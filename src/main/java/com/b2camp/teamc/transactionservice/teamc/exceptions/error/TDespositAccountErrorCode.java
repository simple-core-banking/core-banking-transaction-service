package com.b2camp.teamc.transactionservice.teamc.exceptions.error;

public enum TDespositAccountErrorCode {

    DEPOSIT_ACCOUNT_NOT_FOUND,
    DEPOSIT_ACCOUNT_EMPTY,
    DEPOSIT_ACCOUNT_IS_EXISTS,
    DEPOSIT_ACCOUNT_ID_NOT_FOUND;
}
