package com.b2camp.teamc.transactionservice.teamc.models;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_saving_account_detail")
@SuperBuilder
public class TSavingAccountDetail extends BaseEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", unique = true, nullable = false, length = 50)
        private Long id;

        @ManyToOne(targetEntity = TSavingAccount.class,cascade = CascadeType.ALL)
        @JoinColumn(name = "saving_account_id", referencedColumnName = "id")
        private TSavingAccount tSavingAccount;

        @Column(name = "mutation", nullable = false)
        private String mutation;

        @Column(name = "transaction_amount", nullable = false)
        private BigDecimal transactionAmount;

        @ManyToOne(targetEntity = MTransactionCode.class,cascade = CascadeType.ALL)
        @JoinColumn(name = "m_transaction_code_id", referencedColumnName = "id")
        private MTransactionCode mtransactionCode;

        @Column(name="ref_number",nullable = false)
        private String ref;
}
