package com.b2camp.teamc.transactionservice.teamc.services.impl;

import com.b2camp.teamc.transactionservice.teamc.constant.Constant;
import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TDepositAccountDetailRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TDepositAccountDetailResponse;
import com.b2camp.teamc.transactionservice.teamc.exceptions.BusinessException;
import com.b2camp.teamc.transactionservice.teamc.exceptions.error.TDepositAccountErrorMessage;
import com.b2camp.teamc.transactionservice.teamc.exceptions.error.TDespositAccountErrorCode;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccountDetail;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccountDetail;
import com.b2camp.teamc.transactionservice.teamc.models.references.MDepositProduct;
import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import com.b2camp.teamc.transactionservice.teamc.repositories.*;
import com.b2camp.teamc.transactionservice.teamc.services.TDepositAccountDetailService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class TDepositAccountDetailServiceImpl implements TDepositAccountDetailService {


    private TDepositAccountDetailRepo tDepositAccountDetailRepo;
    private TDepositAccountRepo tDepositAccountRepo;
    private MTransactionCodeRepo mTransactionCodeRepo;
    private ModelMapper mapper;
    private SavingAccountDetailRepo tSavingAccountDetailRepo;
    private TSavingAccountRepo tSavingAccountRepo;
    private MDepositProductRepo mDepositProductRepo;

    @Override
    @Transactional
    @SneakyThrows
    public TDepositAccountDetailResponse add(TDepositAccountDetailRequest tDepositAccountDetailRequest) {
        Optional<MTransactionCode> checkTranSactionCode = getTransactionCode(tDepositAccountDetailRequest);

        Optional<TSavingAccount> checkSavingAccount = getSavingAccount(tDepositAccountDetailRequest);

        Optional<TDepositAccount> checkDepositAccount = getDepositAccount(tDepositAccountDetailRequest, checkSavingAccount);

        TDepositAccountDetail depositDetail = TDepositAccountDetail.builder()
                .mtransactionCode(checkTranSactionCode.get())
                .tDepositAccount(checkDepositAccount.get())
                .transactionAmount(tDepositAccountDetailRequest.getTransactionAmmount())
                .mutationStatus(tDepositAccountDetailRequest.getMutationType())
                .build();
        TDepositAccountDetail savingDepositDetail = tDepositAccountDetailRepo.save(depositDetail);

        TSavingAccountDetail saveDataSavingDtl = TSavingAccountDetail.builder()
                .id(savingDepositDetail.getId())
                .mutation(savingDepositDetail.getRef())
                .transactionAmount(savingDepositDetail.getTransactionAmount())
                .mtransactionCode(savingDepositDetail.getMtransactionCode())
                .build();

        TSavingAccountDetail savingAccountDtl = tSavingAccountDetailRepo.save(saveDataSavingDtl);

        return TDepositAccountDetailResponse.builder()
                .id(savingAccountDtl.getId())
                .transactionAmmount(savingAccountDtl.getTransactionAmount())
                .tDepositAccount(savingDepositDetail.getTDepositAccount())
                .transactionCode(savingAccountDtl.getMtransactionCode())
                .mutationType(savingDepositDetail.getMutationStatus())
                .tSavingAccount(savingDepositDetail.getTDepositAccount().getTSavingAccount())
                .build();

    }

    private Optional<TDepositAccount> getDepositAccount(TDepositAccountDetailRequest tDepositAccountDetailRequest, Optional<TSavingAccount> checkSavingAccount) throws Exception {
        var checkDepositAccount =
                tDepositAccountRepo.findByAccountNumber(tDepositAccountDetailRequest.getTDepositAccount());
        if (checkDepositAccount.isEmpty()) {
            throw new Exception("Deposit Account Not Found");
        }

        if (checkSavingAccount.get().getBalance().compareTo(tDepositAccountDetailRequest.getTransactionAmmount()) <= 0) {
            throw new Exception("Saldo Kurang");
        }
        return checkDepositAccount;
    }

    private Optional<TSavingAccount> getSavingAccount(TDepositAccountDetailRequest tDepositAccountDetailRequest) throws Exception {
        var checkSavingAccount =
                tSavingAccountRepo.findByAccountNumber(tDepositAccountDetailRequest.getTSavingAccount());
        if (checkSavingAccount.isEmpty()) {
            throw new Exception("Not Found Saving Account");
        }
        return checkSavingAccount;
    }

    private Optional<MTransactionCode> getTransactionCode(TDepositAccountDetailRequest tDepositAccountDetailRequest) throws Exception {
        var checkTranSactionCode =
                mTransactionCodeRepo.findByTransactionCode(tDepositAccountDetailRequest.getTransactionCode());

        if (checkTranSactionCode.isEmpty()) {
            throw new Exception("Code Transaction not found");
        }
        return checkTranSactionCode;
    }

    @Override
    @SneakyThrows
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = EntityNotFoundException.class,
            isolation = Isolation.READ_COMMITTED)
    public TDepositAccountDetail creditAmmount(TDepositAccountDetailRequest tDepositAccountDetailRequest,
                                               BigDecimal creditAmmount) {
        assert (creditAmmount.compareTo(BigDecimal.ZERO) == 1);//assert greater than 0
        Optional<TDepositAccountDetail> tDepositAccountDetailinDb = tDepositAccountDetailRepo.
                findById(Long.valueOf(tDepositAccountDetailRequest.getTDepositAccount()));
        if (tDepositAccountDetailinDb.isPresent()) {
            TDepositAccountDetail tDepositAccountDetail = tDepositAccountDetailinDb.get();
            tDepositAccountDetail.credit(creditAmmount);
            return tDepositAccountDetailRepo.save(tDepositAccountDetail);
        }
        throw new Exception("Deposit not Found");
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, isolation = Isolation.READ_COMMITTED)
    @SneakyThrows
    public TDepositAccountDetail debitAmmount(TDepositAccountDetailRequest tDepositAccountDetailRequest, BigDecimal debitAmmount) {
        assert (debitAmmount.compareTo(BigDecimal.ZERO) == 1);
        Optional<TDepositAccountDetail> tDepositAccountDetailInDb = tDepositAccountDetailRepo.findById(
                Long.valueOf(tDepositAccountDetailRequest.getTDepositAccount()));
        if (tDepositAccountDetailInDb.isPresent()) {
            TDepositAccountDetail tDepositAccountDetail = tDepositAccountDetailInDb.get();
            tDepositAccountDetail.debit(debitAmmount);
            return tDepositAccountDetailRepo.save(tDepositAccountDetail);
        }
        throw new Exception("Saldo Kurang");
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = EntityNotFoundException.class, isolation = Isolation.READ_COMMITTED)
    @SneakyThrows
    public TDepositAccountDetailResponse updateSaldo(Long id, TDepositAccountDetailRequest tDepositAccountDetailRequest) {
        var checkDepositProduct = mDepositProductRepo.findById(id);
        if (checkDepositProduct.isEmpty()) {
            throw new Exception("ID not found");
        }
        var checkDepositAccount = tDepositAccountRepo.findById(id);
        if (checkDepositAccount.isEmpty()) {
            throw new Exception("Deposit Account Not Found");
        }
        MDepositProduct mDepositProduct = checkDepositProduct.get();
        TDepositAccount depositAccount = checkDepositAccount.get();
        BigDecimal updateSaldo = depositAccount.getNominal().multiply(BigDecimal
                .valueOf(mDepositProduct.getInterestRate()).multiply(BigDecimal.valueOf(depositAccount.getTimePeriod())));

        TDepositAccountDetail checkDepositDtl = tDepositAccountDetailRepo.findById(id).get();
        checkDepositDtl.setTransactionAmount(updateSaldo);

        TDepositAccountDetail savingDepositDtl = tDepositAccountDetailRepo.save(checkDepositDtl);

        TDepositAccountDetailResponse sendResponse = TDepositAccountDetailResponse.builder()
                .id(savingDepositDtl.getId())
                .tSavingAccount(savingDepositDtl.getTDepositAccount().getTSavingAccount())
                .transactionCode(savingDepositDtl.getMtransactionCode())
                .mutationType(savingDepositDtl.getMutationStatus())
                .transactionAmmount(savingDepositDtl.getTransactionAmount())
                .transactionCode(savingDepositDtl.getMtransactionCode())
                .build();

        return sendResponse;
    }

    @Override
    @Transactional
    @SneakyThrows
    public TDepositAccountDetail createTDepositAccountDetail(TDepositAccountDetailRequest tDepositAccountDetailRequest) {
    /*    var transactionCode = mTransactionCodeRepo.findByTransactionCode(transactionRequest.getTransactionCode());
        if (transactionCode.isPresent()) {
            var checkDeposit = tDepositAccountRepo.findByAccountNumber(transactionRequest.getAccNumberDestination());
            if (checkDeposit.isPresent()) {
                var dataCheck = tSavingAccountRepo.findByAccountNumber(transactionRequest.getAccNumberSource());
                if (dataCheck.isPresent()) {
                    if (dataCheck.get().getBalance().compareTo(transactionRequest.getTotalAmount()) >= 0) {
                        TSavingAccountDetail getReq = TSavingAccountDetail.builder()
                                .tSavingAccount(dataCheck.get())
                                .mutation("DEBIT")
                                .mtransactionCode(transactionCode.get())
                                .ref(ambilRef())
                                .transactionAmount(transactionRequest.getTotalAmount())
                                .deleted(Boolean.FALSE)
                                .build();

                        var saveTSavingAccountDetail = tSavingAccountDetailRepo.save(getReq);

                        TDepositAccountDetail setEntityTDepositAccountDetail = TDepositAccountDetail.builder()
                                .tDepositAccount(tDepositAccountRepo.findByAccountNumber(transactionRequest.getAccNumberDestination()).get())
                                .mtransactionCode(mTransactionCodeRepo.findByTransactionCode(transactionRequest.getTransactionCode()).get())
                                .deleted(Boolean.FALSE)
                                .mutationStatus(Mutation.CREDIT)
                                .transactionAmount(transactionRequest.getTotalAmount())
                                .ref(saveTSavingAccountDetail.getRef())
                                .build();

                        var saveDepositAccountDetail = tDepositAccountDetailRepo.save(setEntityTDepositAccountDetail);
                        updateSaldoDeposit(saveDepositAccountDetail.getId(), saveDepositAccountDetail.getTransactionAmount(), saveTSavingAccountDetail.getMutation());
                        updateSaldoSaving(getReq.getId(), getReq.getTransactionAmount(), getReq.getMutation());

                        return List.of(saveDepositAccountDetail);
                    }
                    return List.of(HttpStatus.NOT_ACCEPTABLE.getReasonPhrase().concat("Saldo tidak cukup"));
                }
                return List.of(HttpStatus.NOT_FOUND.getReasonPhrase().concat("Nomor Rekening Not Found"));
            }
            return List.of(HttpStatus.NOT_FOUND.getReasonPhrase().concat("Nomor deposit not found"));
        }
        return List.of(HttpStatus.NOT_FOUND.getReasonPhrase().concat("kode not found"));*/
        Optional<TDepositAccount> checkAccount = tDepositAccountRepo.findByAccountNumber(tDepositAccountDetailRequest.getTDepositAccount());
        Optional<TSavingAccount> checkSavingAccount = tSavingAccountRepo.findByAccountNumber(tDepositAccountDetailRequest.getTSavingAccount());
        Optional<MTransactionCode> checkTransactionCode = mTransactionCodeRepo.findByTransactionCode(tDepositAccountDetailRequest.getTransactionCode());
        if (checkAccount.isPresent()) {
            if (checkSavingAccount.isPresent()) {
                if (checkTransactionCode.isPresent()) {
                    TDepositAccountDetail tDepositAccountDetail = mapper.map(tDepositAccountDetailRequest, TDepositAccountDetail.class);
                    tDepositAccountDetail.setTDepositAccount(checkAccount.get());
                    tDepositAccountDetail.setMutationStatus(tDepositAccountDetailRequest.getMutationType());
                    tDepositAccountDetail.setTransactionAmount(checkAccount.get().getNominal());
                    tDepositAccountDetail.setMtransactionCode(checkTransactionCode.get());
                    return tDepositAccountDetailRepo.save(tDepositAccountDetail);
                }
                throw new BusinessException(
                        new TDepositAccountErrorMessage(
                                Map.of(TDespositAccountErrorCode.DEPOSIT_ACCOUNT_ID_NOT_FOUND, new Object()),
                                TDespositAccountErrorCode.DEPOSIT_ACCOUNT_NOT_FOUND));
            }
            throw new Exception(" Tsaving Account Not Found");
        }
        throw new Exception("Transaction Code not Found");

    }


    @Override
    public Iterable<TDepositAccountDetail> findAllDataTDepositAccountDetail() {
        return tDepositAccountDetailRepo.findAll();
    }

    @Override
    public Map<String, Object> softDeleteTDepositAccountDetail(Long id) {
        Map<String, Object> softDelete = new HashMap<>();
        try {
            TDepositAccountDetail dataDepositDetail = tDepositAccountDetailRepo.findByIdTrue(id);
            dataDepositDetail.setDeleted(Boolean.TRUE);

            tDepositAccountDetailRepo.save(dataDepositDetail);
            softDelete.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            softDelete.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            softDelete.put(Constant.DATA_STRING, "Deleted");
        } catch (Exception e) {
            softDelete.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            softDelete.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return softDelete;
    }

    private String ambilRef() {
        var hasilRefSaving = tSavingAccountDetailRepo.findRefByRefOrderByIdDescLimit1();
        var hasilRefDeposit = tDepositAccountDetailRepo.findRefByRefOrderByIdDescLimit1();
        String output = null;
        if (hasilRefSaving.isEmpty() && hasilRefDeposit.isEmpty()) {
            output = "1";
        } else if (hasilRefSaving.isPresent() && hasilRefDeposit.isEmpty()) {
            int refSaving = Integer.valueOf(hasilRefSaving.get().getRef()) + 1;
            output = String.valueOf(refSaving);
        } else if (hasilRefSaving.isEmpty() && hasilRefDeposit.isPresent()) {
            int refDeposit = Integer.valueOf(hasilRefDeposit.get().getRef()) + 1;
            output = String.valueOf(refDeposit);
        } else if (hasilRefSaving.isPresent() && hasilRefDeposit.isPresent()) {
            int refSaving = Integer.parseInt(hasilRefSaving.get().getRef());
            int refDeposit = Integer.parseInt(hasilRefDeposit.get().getRef());
            output = String.valueOf(refSaving > refDeposit ? refSaving + 1 : refDeposit + 1);
        }
        return output;
    }

    @Transactional
    private void updateSaldoDeposit(Long id, BigDecimal saldo, String mutation) {
        var update = tDepositAccountRepo.findById(id);
        if (mutation.equals("DEBIT")) {
            update.get().setNominal(update.get().getNominal().subtract(saldo));
            tDepositAccountRepo.save(update.get());
        } else {
            update.get().setNominal(update.get().getNominal().add(saldo));
            tDepositAccountRepo.save(update.get());
        }
    }

    @Transactional
    private void updateSaldoSaving(Long id, BigDecimal saldo, String mutation) {
        var update = tSavingAccountRepo.findById(id);
        if (mutation.equals("DEBIT")) {
            update.get().setBalance(update.get().getBalance().subtract(saldo));
            tSavingAccountRepo.save(update.get());
        } else {
            update.get().setBalance(update.get().getBalance().add(saldo));
            tSavingAccountRepo.save(update.get());
        }
    }


}