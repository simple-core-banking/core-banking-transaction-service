package com.b2camp.teamc.transactionservice.teamc.controllers;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TJournalLedgerRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TJournalLedgerResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TJournalLedger;
import com.b2camp.teamc.transactionservice.teamc.services.TJournalLedgerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tjournalledger")
public class TJournalLedgerController {

    @Autowired private TJournalLedgerService service;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public TJournalLedgerResponse add(@RequestBody TJournalLedgerRequest request){
        return service.add(request);
    }

    @GetMapping("/findAll")
    @ResponseStatus(HttpStatus.CREATED)
    public List<TJournalLedger> findAll(){
        return service.findAll();
    }

    @DeleteMapping("/delete/{id}")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

    @PutMapping(value = "/{id}")
    public TJournalLedgerResponse update(@PathVariable(value = "id") Long id, @RequestBody TJournalLedgerRequest request) {
        return service.update(id, request);
    }
    @GetMapping("/{id}")
    TJournalLedger findById(@PathVariable("id") Long id){
        return service.getById(id);
    }

}
