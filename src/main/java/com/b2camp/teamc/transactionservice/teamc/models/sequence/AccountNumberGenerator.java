package com.b2camp.teamc.transactionservice.teamc.models.sequence;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;

public class AccountNumberGenerator extends SequenceStyleGenerator {

    public static final String ACCOUNT_NUMBER_FORMAT_PARAMETER = "accountNumberFormat";
    public static final String ACCOUNT_NUMBER_FORMAT_DEFAULT = "%d";

    private long accountNumberFormat;

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return "DAC" +String.format(String.valueOf(accountNumberFormat), super.generate(session, object));
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        super.configure(LongType.INSTANCE, params, serviceRegistry);

        accountNumberFormat = ConfigurationHelper.getLong(ACCOUNT_NUMBER_FORMAT_PARAMETER,params, Integer.parseInt(ACCOUNT_NUMBER_FORMAT_DEFAULT));
    }
}
