package com.b2camp.teamc.transactionservice.teamc.dtos.responses;

import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SavingTransactionResponse {
    private Long id;
    private TSavingAccount tSavingAccount;
    private String mutation;
    private BigDecimal transactionAmount;
    private MTransactionCode mtransactionCode;
    private String ref;
}
