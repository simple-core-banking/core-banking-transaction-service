package com.b2camp.teamc.transactionservice.teamc.exceptions.error;

import com.b2camp.teamc.transactionservice.teamc.exceptions.GlobalApplicationMessage;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.util.Map;

@AllArgsConstructor
public class TDepositAccountErrorMessage implements GlobalApplicationMessage {

    private final Map<TDespositAccountErrorCode, Object> dataErrorTDepositAccount;
    private final TDespositAccountErrorCode tDespositAccountErrorCode;

    @Override
    public String getCode() {
        return tDespositAccountErrorCode.name();
    }

    @SneakyThrows
    @Override
    public String getMessage() {
        switch (tDespositAccountErrorCode){
            case DEPOSIT_ACCOUNT_EMPTY:
                return dataDepositAccountEmpty();
            case DEPOSIT_ACCOUNT_IS_EXISTS:
                return depostAccountIsExists();
            case DEPOSIT_ACCOUNT_ID_NOT_FOUND:
                return errorIdNotFound();
            case DEPOSIT_ACCOUNT_NOT_FOUND:
                return errorDataNotFound();
            default:
                throw new Exception(getMessage());
        }

    }

    private String errorDataNotFound() {
        return " Deposit Account Not Found " +
                dataErrorTDepositAccount.get(TDespositAccountErrorCode.DEPOSIT_ACCOUNT_NOT_FOUND);

    }

    private String errorIdNotFound() {
        return " ID Deposit Account Not Found " +
                dataErrorTDepositAccount.get(TDespositAccountErrorCode.DEPOSIT_ACCOUNT_ID_NOT_FOUND);

    }

    private String dataDepositAccountEmpty() {
        return " Deposit Account is not Empty " +
                dataErrorTDepositAccount.get(TDespositAccountErrorCode.DEPOSIT_ACCOUNT_EMPTY);
    }

    private String depostAccountIsExists(){
        return " Deposit Account is already Exists " +
                dataErrorTDepositAccount.get(TDespositAccountErrorCode.DEPOSIT_ACCOUNT_IS_EXISTS);

    }
}
