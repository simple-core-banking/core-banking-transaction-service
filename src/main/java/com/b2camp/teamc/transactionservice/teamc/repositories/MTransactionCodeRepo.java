package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MTransactionCodeRepo extends JpaRepository<MTransactionCode,Long> {
    Optional<MTransactionCode> findByTransactionCode(String transactionCode);
}
