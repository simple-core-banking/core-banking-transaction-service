package com.b2camp.teamc.transactionservice.teamc.helper;

import org.apache.commons.lang3.StringUtils;

public abstract class MBilyetGenerator {
    public static String generateMBilyetGenerator(String bilyetNumber, Integer jumlahDigit) {
        if (!StringUtils.isNumeric(String.valueOf(bilyetNumber))) {
            throw new IllegalArgumentException("Nomor Bilyet [" + bilyetNumber + "] tidak numerik");
        }
        if (String.valueOf(bilyetNumber).length() < jumlahDigit) {
            return StringUtils.rightPad(String.valueOf(Integer.valueOf(bilyetNumber)), jumlahDigit, "0");
        } else {
            return String.valueOf(bilyetNumber).substring(bilyetNumber.length());
        }

    }
}
