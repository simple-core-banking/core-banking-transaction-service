package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.TJournalLedger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TJournalLedgerRepo extends JpaRepository<TJournalLedger, Long> {
    @Query(value="SELECT * FROM t_journal_ledger tjl "
            + "WHERE tjl.id = :id " + "AND tjl.is_deleted = false", nativeQuery= true)
    TJournalLedger findByIdTrue(@Param("id") Long id);
}
