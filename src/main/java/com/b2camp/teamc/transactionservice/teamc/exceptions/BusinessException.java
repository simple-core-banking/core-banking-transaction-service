package com.b2camp.teamc.transactionservice.teamc.exceptions;

public class BusinessException extends RuntimeException {

    private final GlobalApplicationMessage message;

    public BusinessException(GlobalApplicationMessage message) {
        super(message.getMessage());
        this.message = message;
    }

    @Override
    public String toString() {
        return message.getCode();
    }
}
