package com.b2camp.teamc.transactionservice.teamc.models;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import com.b2camp.teamc.transactionservice.teamc.models.references.MCif;
import com.b2camp.teamc.transactionservice.teamc.models.references.MDepositProduct;
import com.b2camp.teamc.transactionservice.teamc.models.references.RStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_deposit_account")
@SuperBuilder
public class TDepositAccount extends BaseEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id",unique = true, nullable = false,length = 50)
        private Long id;

        @OneToOne(targetEntity = TSavingAccount.class,cascade =CascadeType.ALL)
        @JoinColumn(name = "t_saving_account_id")
        private TSavingAccount tSavingAccount;

        @Column(name = "nominal" , nullable = false)
        private BigDecimal nominal;

        @ManyToOne(targetEntity = MCif.class,cascade=CascadeType.ALL)
        @JoinColumn(name = "cif_id",referencedColumnName = "id")
        private MCif mCif;

        @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "account_number_seq")
        @GenericGenerator(name = "account_number_seq",strategy = "package com.b2camp.teamc.transactionservice.teamc.models.sequence.AccountNumberGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "initial_value",value = "1"),
                @org.hibernate.annotations.Parameter(name = "increment_siza",value = "1")
        })
        @Column(name = "account_number",unique = true)
        private String accountNumber;

        @ManyToOne(targetEntity = MDepositProduct.class,cascade = CascadeType.ALL)
        @JoinColumn(name = "product_id",referencedColumnName = "id")
        private MDepositProduct mDepositProduct;

        @Column(name = "time_period", nullable = false)
        private Integer timePeriod;

        @Column(name = "start_date", nullable = false)
        private Date startDate;

        @Column(name = "due_date")
        private Date dueDate;

        @ManyToOne
        @JoinColumn(name = "status_id", referencedColumnName = "id")
        private RStatus rStatus;

       /* @NotNull @Enumerated(EnumType.STRING)
        private StatusDeposito statusDeposito = StatusDeposito.AKTIF;

        @NotNull @Enumerated(EnumType.STRING)
        private StatusNotification statusNotification = StatusNotification.BELUM_BERUBAH;*/

        @Override
        public String toString() {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                return String.format("{ Nominal : %s , Account Number : %s, Time Period : %d ,Start Date : %s , Due date : %s }"
                ,nominal,accountNumber,timePeriod,sdf.format(startDate.toString()),sdf.format(dueDate.toString()));
                /*return "TDepositAccount{" +
                        "nominal=" + nominal +
                        ", accountNumber='" + accountNumber + '\'' +
                        ", timePeriod=" + timePeriod +
                        ", startDate=" + startDate +
                        ", dueDate=" + dueDate +
                        '}';*/
        }
}
