package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.references.MSavingProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MSavingProductRepo extends JpaRepository<MSavingProduct, Long> {
}
