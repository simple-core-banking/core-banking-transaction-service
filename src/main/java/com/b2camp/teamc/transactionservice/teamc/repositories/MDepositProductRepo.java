package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.references.MDepositProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MDepositProductRepo extends JpaRepository<MDepositProduct,Long> {
}
