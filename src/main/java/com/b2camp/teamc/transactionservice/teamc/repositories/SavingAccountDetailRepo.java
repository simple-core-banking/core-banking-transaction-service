package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccountDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface SavingAccountDetailRepo  extends JpaRepository<TSavingAccountDetail, Long> {
    @Query(value = "Select * from t_saving_account_detail tsad order by tsad.id desc limit 1",nativeQuery = true)
    Optional<TSavingAccountDetail> findRefByRefOrderByIdDescLimit1();
    Optional<TSavingAccountDetail> findByRef(String ref);
}
