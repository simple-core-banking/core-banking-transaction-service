package com.b2camp.teamc.transactionservice.teamc.controllers;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TransactionRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.SavingTransactionResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import com.b2camp.teamc.transactionservice.teamc.repositories.TSavingAccountRepo;
import com.b2camp.teamc.transactionservice.teamc.services.TSavingAccountDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transaction/saving")
public class SavingTransactionController {
    @Autowired
    private TSavingAccountDetailService service;

    @PostMapping
    public Object transaction(@RequestBody(required = false) TransactionRequest request){
        return service.transaction(request);
    }
    @GetMapping
    List<SavingTransactionResponse> report(){
        return service.report();
    }
    @GetMapping("/{ref}")
    SavingTransactionResponse reportByRef(@RequestParam("ref") String ref){
        return service.reportByRef(ref);
    }

}
