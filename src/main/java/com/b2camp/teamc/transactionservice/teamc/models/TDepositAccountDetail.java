package com.b2camp.teamc.transactionservice.teamc.models;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="t_deposit_account_detail")
@SuperBuilder
public class TDepositAccountDetail extends BaseEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id",unique = true, nullable = false,length = 50)
        private Long id;

        @ManyToOne(targetEntity = TDepositAccount.class,cascade = CascadeType.ALL)
        @JoinColumn(name = "deposit_account_id",referencedColumnName = "id")
        private TDepositAccount tDepositAccount;


        @Enumerated(EnumType.STRING)
        private Mutation mutationStatus;

        @Column(name = "transaction_amount", nullable = false)
        private BigDecimal transactionAmount;


        @ManyToOne(targetEntity = MTransactionCode.class,cascade = CascadeType.ALL)
        @JoinColumn(name = "transaction_code_id", referencedColumnName = "id")
        private MTransactionCode mtransactionCode;


        @Column(name="ref_number")
        private String ref;

        /**
         * Mengkreditkan atau melakukan kredit ke jumlah yang diberikan ke saldo akun berjalan
         * @param creditAmount
         */
        public void credit(BigDecimal creditAmount) {
                this.transactionAmount = this.transactionAmount.add(creditAmount);
        }

        /**
         *
         * Mendebit jumlah yang diberikan dari saldo akun saat ini.
         * @param debitAmount
         * @throws Exception
         */
        public void debit(BigDecimal debitAmount) throws Exception {
                if(this.transactionAmount.compareTo(debitAmount) >= 0){
                        this.transactionAmount = this.transactionAmount.subtract(debitAmount);
                        return;
                }
                throw new Exception("Saldo tidak Cukup");
        }
    }
