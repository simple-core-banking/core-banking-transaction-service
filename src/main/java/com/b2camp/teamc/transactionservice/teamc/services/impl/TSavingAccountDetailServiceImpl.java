package com.b2camp.teamc.transactionservice.teamc.services.impl;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TransactionRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.SavingTransactionResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccountDetail;
import com.b2camp.teamc.transactionservice.teamc.repositories.MTransactionCodeRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.SavingAccountDetailRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.TSavingAccountRepo;
import com.b2camp.teamc.transactionservice.teamc.services.TSavingAccountDetailService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TSavingAccountDetailServiceImpl implements TSavingAccountDetailService {

    private TSavingAccountRepo savingAccountRepo;

    private SavingAccountDetailRepo detailRepository;

    private MTransactionCodeRepo codeRepo;

    private ModelMapper mapper;

    public SavingTransactionResponse entityToResponse(TSavingAccountDetail entity){
        return mapper.map(entity,SavingTransactionResponse.class);
    }

    @Override
    public Object transaction(TransactionRequest request) {
        var validTransactionCode = codeRepo.findByTransactionCode(request.getTransactionCode());
        if (validTransactionCode.isPresent()) {
            var transactionCode = validTransactionCode.get().getTransactionCode();
            if (transactionCode.equals("TR1")) {
                return transactionSetorTunai(request);
            } else if (transactionCode.equals("TR2")) {
                return transactionTarikTunai(request);
            }
            else if(transactionCode.equals("TR3")){
                return transactionOverBook(request);
            }
        }
        return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase().concat(" Reason :Transaction Code Not Found");
    }

    @Transactional
    public Object transactionTarikTunai(TransactionRequest request) {
        var dataCheck=savingAccountRepo.findByAccountNumber(request.getAccNumberSource());
        if(dataCheck.isPresent()&&dataCheck.get().getBalance().compareTo(request.getTotalAmount())>=0){
        TSavingAccountDetail getReq = TSavingAccountDetail.builder()
                .tSavingAccount(dataCheck.get())
                .mutation("DEBIT")
                .mtransactionCode(codeRepo.findByTransactionCode(request.getTransactionCode()).get())
                .ref(ambilRef())
                .transactionAmount(request.getTotalAmount())
                .deleted(Boolean.FALSE)
                .build();
        TSavingAccountDetail save = detailRepository.save(getReq);
        updateSaldo(save.getTSavingAccount().getId(), save.getTransactionAmount(), save.getMutation());
        return entityToResponse(save);}
        return HttpStatus.NOT_FOUND;
    }
    @Transactional
    private Object transactionSetorTunai(TransactionRequest request) {
        var dataCheck=savingAccountRepo.findByAccountNumber(request.getAccNumberDestination());
        if(dataCheck.isPresent()){
        TSavingAccountDetail getReq = TSavingAccountDetail.builder()
                .tSavingAccount(dataCheck.get())
                .mutation("CREDIT")
                .mtransactionCode(codeRepo.findByTransactionCode(request.getTransactionCode()).get())
                .ref(ambilRef())
                .transactionAmount(request.getTotalAmount())
                .deleted(Boolean.FALSE)
                .build();
        TSavingAccountDetail save = detailRepository.save(getReq);
        updateSaldo(save.getTSavingAccount().getId(), save.getTransactionAmount(), save.getMutation());
        return entityToResponse(save);}
        return HttpStatus.NOT_FOUND;
    }
    @Transactional
    public List<Object> transactionOverBook(TransactionRequest request) {
        var dataCheck = savingAccountRepo.findByAccountNumberIn(List.of(request.getAccNumberDestination(), request.getAccNumberSource()));
        if (dataCheck.isPresent()&&dataCheck.get().get(1).getBalance().compareTo(request.getTotalAmount())>=0) {
            TSavingAccountDetail getSource = TSavingAccountDetail.builder()
                    .tSavingAccount(dataCheck.get().get(1))
                    .mutation("DEBIT")
                    .mtransactionCode(codeRepo.findByTransactionCode(request.getTransactionCode()).get())
                    .ref(ambilRef())
                    .transactionAmount(request.getTotalAmount())
                    .deleted(Boolean.FALSE)
                    .build();

            TSavingAccountDetail getDestination = TSavingAccountDetail.builder()
                    .tSavingAccount(dataCheck.get().get(0))
                    .mutation("CREDIT")
                    .mtransactionCode(codeRepo.findByTransactionCode(request.getTransactionCode()).get())
                    .ref(ambilRef())
                    .transactionAmount(request.getTotalAmount())
                    .deleted(Boolean.FALSE)
                    .build();
            var saveAll = detailRepository.saveAll(List.of(getSource, getDestination));
            updateSaldo(saveAll.get(0).getTSavingAccount().getId(), saveAll.get(0).getTransactionAmount(), saveAll.get(0).getMutation());
            updateSaldo(saveAll.get(1).getTSavingAccount().getId(), saveAll.get(1).getTransactionAmount(), saveAll.get(1).getMutation());
            return List.of(entityToResponse(getSource), entityToResponse(getDestination));}
        return List.of(HttpStatus.NOT_FOUND);
    }
    @Transactional
    public void updateSaldo(Long id, BigDecimal saldo, String mutation) {
        var update = savingAccountRepo.findById(id);
        if (mutation.equals("DEBIT")) {
            update.get().setBalance(update.get().getBalance().subtract(saldo));
            savingAccountRepo.save(update.get());
        }
        else{
        update.get().setBalance(update.get().getBalance().add(saldo));
        savingAccountRepo.save(update.get());}
    }


    public String ambilRef(){
        var hasilRef= detailRepository.findRefByRefOrderByIdDescLimit1();
        if(hasilRef.isPresent()){
            int a= Integer.valueOf(hasilRef.get().getRef()) + 1;
            return String.valueOf(a);
        }
        return "1";
    }

    @Override
    public List<SavingTransactionResponse> report() {
        var listEntity = detailRepository.findAll();

        return listEntity.stream().map(a->SavingTransactionResponse.builder()
                .id(a.getId())
                .tSavingAccount(a.getTSavingAccount())
                .mtransactionCode(a.getMtransactionCode())
                .mutation(a.getMutation())
                .ref(a.getRef())
                .build()).collect(Collectors.toList());
    }

    @Override
    public SavingTransactionResponse reportByRef(String ref) {
        return entityToResponse(detailRepository.findByRef(ref).orElseThrow());
    }
}
