package com.b2camp.teamc.transactionservice.teamc.services.impl;

import com.b2camp.teamc.transactionservice.teamc.constant.Constant;
import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TJournalLedgerRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TJournalLedgerResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TJournalLedger;
import com.b2camp.teamc.transactionservice.teamc.repositories.MTransactionCodeRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.SavingAccountDetailRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.TJournalLedgerRepo;
import com.b2camp.teamc.transactionservice.teamc.services.TJournalLedgerService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class TJournalLedgerServiceImpl implements TJournalLedgerService {

    @Autowired private TJournalLedgerRepo tJournalLedgerRepo;
    @Autowired private MTransactionCodeRepo transactionCodeRepo;
    @Autowired private SavingAccountDetailRepo savingAccountDetailRepo;
    @Autowired
    public EntityManager entityManager;
    @PersistenceUnit
    EntityManagerFactory emf;

    ModelMapper mapper = new ModelMapper();

    @Override
    public TJournalLedgerResponse add(TJournalLedgerRequest request) {
        TJournalLedger tJournalLedger = TJournalLedger.builder()
                .tSavingAccountDetail(savingAccountDetailRepo.findById(request.getTSavingAccountDetail()).get())
                .description(request.getDescription())
                .mTransactionCode(transactionCodeRepo.findById(request.getTransactionCode()).get())
                .nominal(request.getNominal())
                .build();
        TJournalLedger saveJournal = tJournalLedgerRepo.save(tJournalLedger);
        TJournalLedgerResponse response = TJournalLedgerResponse.builder()
                .id(saveJournal.getId())
                .tSavingAccountDetailId(saveJournal.getTSavingAccountDetail())
                .description(saveJournal.getDescription())
                .mTransactionCodeId(saveJournal.getMTransactionCode())
                .nominal(saveJournal.getNominal())
                .build();
        return response;
    }

    @Transactional
    @SneakyThrows
    @Override
    public TJournalLedgerResponse update(Long id, TJournalLedgerRequest request) {
        Optional<TJournalLedger> journalLedger = tJournalLedgerRepo.findById(id);
        if (journalLedger == null){
            throw new Exception("Product with" + id + "Not Found");
        }

        journalLedger.get().setDescription(request.getDescription());
        journalLedger.get().setMTransactionCode(transactionCodeRepo.findById(request.getTransactionCode()).get());
        journalLedger.get().setTSavingAccountDetail(savingAccountDetailRepo.findById(request.getTSavingAccountDetail()).get());
        journalLedger.get().setNominal(request.getNominal());

        TJournalLedger journal = tJournalLedgerRepo.save(journalLedger.get());
        return TJournalLedgerResponse.builder()
                .id(journal.getId())
                .description(journal.getDescription())
                .mTransactionCodeId(journal.getMTransactionCode())
                .tSavingAccountDetailId(journal.getTSavingAccountDetail())
                .nominal(journal.getNominal())
                .build();
    }

    @Transactional
    @SneakyThrows
    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            TJournalLedger data = tJournalLedgerRepo.findByIdTrue(id);
            data.setDeleted(Boolean.TRUE);

            tJournalLedgerRepo.save(data);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, "Deleted");
        }
        catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }


    @Override
    public List<TJournalLedger> findAll() {
        return tJournalLedgerRepo.findAll();
    }

    @Override
    public TJournalLedger getById(Long id) {
        entityManager = emf.createEntityManager();
        log.info("Find Journal Ledger by id"+id);
        return entityManager.find(TJournalLedger.class,id);
    }
}
