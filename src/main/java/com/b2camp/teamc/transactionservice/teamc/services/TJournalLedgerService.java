package com.b2camp.teamc.transactionservice.teamc.services;


import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TJournalLedgerRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TJournalLedgerResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TJournalLedger;

import java.util.List;
import java.util.Map;

public interface TJournalLedgerService {
    TJournalLedgerResponse add(TJournalLedgerRequest request);
    TJournalLedgerResponse update(Long id, TJournalLedgerRequest request);
    Map<String, Object> delete(Long id);
    List<TJournalLedger> findAll();
    TJournalLedger getById(Long id);
}
