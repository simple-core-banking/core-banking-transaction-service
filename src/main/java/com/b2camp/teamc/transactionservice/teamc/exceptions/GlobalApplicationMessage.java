package com.b2camp.teamc.transactionservice.teamc.exceptions;

public interface GlobalApplicationMessage {
    String getCode();
    String getMessage();
}
