package com.b2camp.teamc.transactionservice.teamc.services;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.MDepositBillyetRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.MDepositBillyetResponse;

import java.util.List;

public interface MDepositBillyetService {
    MDepositBillyetResponse create(MDepositBillyetRequest request);
    MDepositBillyetResponse update(Long id, MDepositBillyetRequest request);
    void softDeleted(Long id);
    List<MDepositBillyetResponse> findAll();
    MDepositBillyetResponse findById(Long id);
    MDepositBillyetResponse addDeposit(MDepositBillyetRequest request);
}
