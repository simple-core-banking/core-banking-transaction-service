package com.b2camp.teamc.transactionservice.teamc.dtos.requests;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TJournalLedgerRequest {
    private String description;
    private Long tSavingAccountDetail;
    private Long transactionCode;
    private BigDecimal nominal;
}
