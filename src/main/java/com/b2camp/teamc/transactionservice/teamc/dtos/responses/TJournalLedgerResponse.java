package com.b2camp.teamc.transactionservice.teamc.dtos.responses;

import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccountDetail;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TJournalLedgerResponse {
    private Long id;
    private String description;
    private TSavingAccountDetail tSavingAccountDetailId;
    private MTransactionCode mTransactionCodeId;
    private BigDecimal nominal;
}
