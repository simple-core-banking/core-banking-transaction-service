package com.b2camp.teamc.transactionservice.teamc.controllers;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TSavingAccountRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TSavingAccountResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import com.b2camp.teamc.transactionservice.teamc.services.TSavingAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tsavingaccount")
public class TSavingAccountController {

    @Autowired
    private TSavingAccountService service;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public TSavingAccountResponse add(@RequestBody TSavingAccountRequest request){
        return service.add(request);
    }

    @DeleteMapping("/delete")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return service.delete(id);
    }

    @GetMapping
    public List<TSavingAccount> getJoinInformation(){
        return service.getJoinInformation();
    }

    @GetMapping(value = "/ById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TSavingAccountResponse getById(@PathVariable("id") Long id){
        return service.getById(id);
    }


}
