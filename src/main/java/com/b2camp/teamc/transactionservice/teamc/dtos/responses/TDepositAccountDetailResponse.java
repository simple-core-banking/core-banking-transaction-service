package com.b2camp.teamc.transactionservice.teamc.dtos.responses;

import com.b2camp.teamc.transactionservice.teamc.models.Mutation;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TDepositAccountDetailResponse {

    private Long id;
    private TDepositAccount tDepositAccount;
    private MTransactionCode transactionCode;
    private Mutation mutationType;
    private BigDecimal transactionAmmount;
    private TSavingAccount tSavingAccount;

}
