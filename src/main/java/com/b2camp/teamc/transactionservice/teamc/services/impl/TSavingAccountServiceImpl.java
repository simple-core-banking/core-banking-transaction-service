package com.b2camp.teamc.transactionservice.teamc.services.impl;

import com.b2camp.teamc.transactionservice.teamc.constant.Constant;
import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TSavingAccountRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TSavingAccountResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import com.b2camp.teamc.transactionservice.teamc.repositories.MCifRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.MSavingProductRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.TSavingAccountRepo;
import com.b2camp.teamc.transactionservice.teamc.services.TSavingAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class TSavingAccountServiceImpl implements TSavingAccountService {

    @Autowired
    private TSavingAccountRepo accountRepo;
    @Autowired
    private MCifRepo cifRepo;
    @Autowired
    private MSavingProductRepo savingRapo;

    @Override
    public TSavingAccountResponse add(TSavingAccountRequest request) {
        TSavingAccount tSavingAccount = TSavingAccount.builder()
                .mcif(cifRepo.findById(request.getMcif()).get())
                .mSavingProduct(savingRapo.findById(request.getMSavingProduct()).get())
                .accountNumber(request.getAccountNumber())
                .balance(BigDecimal.valueOf(0))
                .deleted(Boolean.FALSE)
                .build();

        TSavingAccount save = accountRepo.save(tSavingAccount);

        TSavingAccountResponse response = TSavingAccountResponse.builder()
                .id(save.getId())
                .mCifId(save.getMcif())
                .mSavingProductId(save.getMSavingProduct())
                .accountNumber(save.getAccountNumber())
                .balance(save.getBalance())
                .build();

        return response;
    }

    @Transactional
    @Override
    public Map<String, Object> delete(Long id) {
        Map<String, Object> result = new HashMap<>();
        try {
            TSavingAccount data = accountRepo.findByIdTrue(id);
            data.setDeleted(Boolean.TRUE);

            accountRepo.save(data);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, "Deleted");
        }
        catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Transactional
    @Override
    public List<TSavingAccount> getJoinInformation(){
        return accountRepo.findAll();
    }


    @Override
    public TSavingAccountResponse getById(Long id){
        var hasil = accountRepo.findById(id).get();
        return TSavingAccountResponse.builder()
                .id(hasil.getId())
                .mCifId(cifRepo.getById(hasil.getMcif().getId()))
                .accountNumber(hasil.getAccountNumber())
                .mSavingProductId(savingRapo.getById(hasil.getMSavingProduct().getId()))
                .balance(hasil.getBalance())
                .build();
    }


}
