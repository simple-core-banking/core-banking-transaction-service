package com.b2camp.teamc.transactionservice.teamc.controllers;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TDepositAccountRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TDepositAccountResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import com.b2camp.teamc.transactionservice.teamc.services.TDepositAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/tdeposit")
public class TDepositAccountController {

    @Autowired
    private TDepositAccountService tDepositAccountService;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public TDepositAccountResponse add(@RequestBody TDepositAccountRequest request){
        return tDepositAccountService.add(request);
    }


    @PutMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public void update( ) {
        tDepositAccountService.update();
    }

    @DeleteMapping("/delete/{id}")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return tDepositAccountService.delete(id);
    }

    @GetMapping
    public List<TDepositAccount> getJoinInformation(){
        return tDepositAccountService.getJoinInformation();
    }
    @GetMapping(value = "/ById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TDepositAccountResponse getById(@PathVariable("id") Long id){
        return tDepositAccountService.getById(id);
    }
}
