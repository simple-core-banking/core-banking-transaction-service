package com.b2camp.teamc.transactionservice.teamc.controllers;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.MDepositBillyetRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.MDepositBillyetResponse;
import com.b2camp.teamc.transactionservice.teamc.services.MDepositBillyetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/MDepositBillyet")
public class MDepositBillyetController {

    @Autowired
    MDepositBillyetService service;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public MDepositBillyetResponse add(@RequestBody MDepositBillyetRequest request){
        return service.addDeposit(request);
    }

    @GetMapping
    List<MDepositBillyetResponse> findAll(){
        return service.findAll();
    }

    @GetMapping("/{id}")
    MDepositBillyetResponse findById(@PathVariable("id") Long id){
        return service.findById(id);
    }

    @DeleteMapping("/{id}")
    void softDeleted(@PathVariable("id") Long id){
        service.softDeleted(id);
    }

    @PutMapping("/{id}")
    MDepositBillyetResponse update(@PathVariable("id")Long id,@RequestBody MDepositBillyetRequest request){
        return service.update(id, request);
    }


}
