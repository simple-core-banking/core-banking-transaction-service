package com.b2camp.teamc.transactionservice.teamc.dtos.requests;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class TransactionRequest {
        private String transactionCode;
        private String accNumberSource;
        private String accNumberDestination;
        private BigDecimal totalAmount;

}
