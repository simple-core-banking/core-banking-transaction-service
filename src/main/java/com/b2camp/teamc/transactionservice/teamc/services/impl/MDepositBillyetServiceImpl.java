package com.b2camp.teamc.transactionservice.teamc.services.impl;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.MDepositBillyetRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.MDepositBillyetResponse;
import com.b2camp.teamc.transactionservice.teamc.helper.MBilyetGenerator;
import com.b2camp.teamc.transactionservice.teamc.models.MDepositBillyet;
import com.b2camp.teamc.transactionservice.teamc.repositories.MDepositBillyetRepo;
import com.b2camp.teamc.transactionservice.teamc.repositories.TDepositAccountRepo;
import com.b2camp.teamc.transactionservice.teamc.services.MDepositBillyetService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@RequiredArgsConstructor
public class MDepositBillyetServiceImpl implements MDepositBillyetService {


    private MDepositBillyetRepo mDepositBillyetRepo;


    private TDepositAccountRepo tdmDepositBillyetRepo;

    @Transactional
    @Override
    public MDepositBillyetResponse create(MDepositBillyetRequest request) {
        MDepositBillyet requesToEntity = MDepositBillyet.builder()
                .bilYetNumber(request.getBilYetNumber())
                .tDepositAccount(tdmDepositBillyetRepo.findById(Long.valueOf(request.getTDepositAccount())).get())
                .build();

        MDepositBillyet saveToDb = mDepositBillyetRepo.save(requesToEntity);
        MDepositBillyetResponse entityToResponse = MDepositBillyetResponse.builder()
                .id(saveToDb.getId())
                .bilYetNumber(saveToDb.getBilYetNumber())
                .tDepositAccount(saveToDb.getTDepositAccount())
                .build();
        return entityToResponse;
    }

    @Override
    public MDepositBillyetResponse addDeposit(MDepositBillyetRequest request) {
        MDepositBillyet mDepositBillyetRequest = MDepositBillyet.builder()
                .tDepositAccount(tdmDepositBillyetRepo.findById(Long.valueOf(request.getTDepositAccount())).get())
                .build();
         if (tdmDepositBillyetRepo.existsById(Long.valueOf(request.getTDepositAccount()))){
            mDepositBillyetRequest.setBilYetNumber(String.valueOf(Integer.parseInt(MBilyetGenerator.generateMBilyetGenerator(
                    request.getBilYetNumber(), 9))));
         } else {
            request.setBilYetNumber(String.valueOf(Math.toIntExact(Long.parseLong(request.getTDepositAccount()))));
         }
         MDepositBillyet savetoResponse = mDepositBillyetRepo.save(mDepositBillyetRequest);

        MDepositBillyetResponse entityToResponse = MDepositBillyetResponse.builder()
                .id(savetoResponse.getId())
                .bilYetNumber(savetoResponse.getBilYetNumber())
                .tDepositAccount(savetoResponse.getTDepositAccount())
                .build();
        return entityToResponse;
    }

    @Transactional
    @Override
    public MDepositBillyetResponse update(Long id, MDepositBillyetRequest request) {
        var update = mDepositBillyetRepo.findById(id).get();
        update.setBilYetNumber(request.getBilYetNumber());
        update.setTDepositAccount(tdmDepositBillyetRepo.findById(Long.valueOf(request.getTDepositAccount())).get());

        MDepositBillyetResponse entityToResponse = MDepositBillyetResponse.builder()
                .id(update.getId())
                .bilYetNumber(update.getBilYetNumber())
                .tDepositAccount(update.getTDepositAccount())
                .build();

        return entityToResponse;
    }

    @Transactional
    @Override
    public void softDeleted(Long id) {
        var delete = mDepositBillyetRepo.findById(id).get();
        delete.setDeleted(Boolean.TRUE);
    }

    @Override
    public List<MDepositBillyetResponse> findAll() {
        var hasil = mDepositBillyetRepo.findAll();
        List<MDepositBillyetResponse> response = hasil.stream()
                .map(a -> new MDepositBillyetResponse(a.getId(), a.getBilYetNumber(), a.getTDepositAccount()))
                .collect(Collectors.toList());
        return response;
    }

    @Override
    public MDepositBillyetResponse findById(Long id) {
        var hasil = mDepositBillyetRepo.findById(id).get();
        MDepositBillyetResponse response = MDepositBillyetResponse.builder()
                .id(hasil.getId())
                .bilYetNumber(hasil.getBilYetNumber())
                .tDepositAccount(hasil.getTDepositAccount())
                .build();
        return response;
    }
}
