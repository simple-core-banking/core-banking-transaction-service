package com.b2camp.teamc.transactionservice.teamc.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class TDepositAccountRequest {

    @JsonIgnore
    private BigDecimal nominal;
    private Integer timePeriod;
    @JsonIgnore
    private Date startDate;
    @JsonIgnore
    private Date dueDate;
    private Long mCif;
    private String accountNumber;
    private Long rStatus;
    private Long mDepositProduct;
    private Long savingAccountId;

}
