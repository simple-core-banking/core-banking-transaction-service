package com.b2camp.teamc.transactionservice.teamc.services;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TDepositAccountRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TDepositAccountResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;

import java.util.List;
import java.util.Map;

public interface TDepositAccountService {

    TDepositAccountResponse add(TDepositAccountRequest tDepositAccountRequest);
    void update();
    Map<String, Object> delete(Long id);
    List<TDepositAccount> getJoinInformation();
    TDepositAccountResponse getById(Long id);


}
