package com.b2camp.teamc.transactionservice.teamc.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TSavingAccountRequest {
    private Long mcif;
    private Long mSavingProduct;
    private String accountNumber;

    @JsonIgnore
    private BigDecimal balance;
}
